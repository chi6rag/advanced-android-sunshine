package com.example.android.sunshine.app.custom;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.Button;

import com.example.android.sunshine.app.R;

public class LocationEditTextPreference extends EditTextPreference {
    private static final int MINIMUM_LENGTH = 0;
    private final TypedArray styledAttributes;
    private final int minimumLength;

    public LocationEditTextPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.styledAttributes = context.obtainStyledAttributes(attrs, R.styleable.LocationEditTextPreference, 0, 0);
        try {
            this.minimumLength = styledAttributes.getInt(R.styleable.LocationEditTextPreference_minimumLength, MINIMUM_LENGTH);
            Log.d("chi6rag", "Min Length: " + this.minimumLength);
        } finally {
            this.styledAttributes.recycle();
        }
    }

    @Override
    protected void showDialog(Bundle state) {
        super.showDialog(state);
        Dialog dialog = getDialog();
        if (dialog instanceof AlertDialog) {
            AlertDialog locationPreferenceDialog = (AlertDialog) dialog;
            Button buttonOk = locationPreferenceDialog.getButton(AlertDialog.BUTTON_POSITIVE);
            addLocationPreferenceTextChangedListener(buttonOk);
        }
    }

    private void addLocationPreferenceTextChangedListener(final Button buttonOk) {
        LocationPreferenceTextChangedListener listener = new LocationPreferenceTextChangedListener(buttonOk, minimumLength);
        getEditText().addTextChangedListener(listener);
    }

}
