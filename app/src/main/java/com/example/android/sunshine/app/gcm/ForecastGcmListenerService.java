package com.example.android.sunshine.app.gcm;

import android.app.NotificationManager;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.bumptech.glide.Glide;
import com.example.android.sunshine.app.R;
import com.example.android.sunshine.app.Utility;
import com.google.android.gms.gcm.GcmListenerService;

import org.json.JSONException;

import java.util.concurrent.ExecutionException;

public class ForecastGcmListenerService extends GcmListenerService {

    private static final String DATA = "data";
    private static final String SUMMARY = "summary";
    private static final String TITLE = "title";
    private static final String ICON = "icon";
    private static final String WEATHER_ID = "weather_id";

    @Override
    public void onMessageReceived(String from, Bundle data) {
        super.onMessageReceived(from, data);
        if (!getString(R.string.gcm_defaultSenderId).equals(from)) return;

        Log.d("chi6rag", "message received from " + data.toString());
        try {
            showWeatherNotification(data);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    private void showWeatherNotification(Bundle data) throws JSONException, ExecutionException, InterruptedException {
        String summary = data.getString(SUMMARY);
        String title = data.getString(TITLE);
        int weatherId = Integer.parseInt(data.getString(WEATHER_ID));

        int largeIconWidth = Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ?
                getResources().getDimensionPixelSize(android.R.dimen.notification_large_icon_width) :
                getResources().getDimensionPixelSize(R.dimen.notification_large_icon_default);

        int largeIconHeight = Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ?
                getResources().getDimensionPixelSize(android.R.dimen.notification_large_icon_height) :
                getResources().getDimensionPixelSize(R.dimen.notification_large_icon_default);

        int artResourceForWeatherCondition = Utility.getArtResourceForWeatherCondition(weatherId);
        String artUrlForWeatherCondition = Utility.getArtUrlForWeatherCondition(this, weatherId);
        Bitmap downloadedLargeIcon = Glide.with(this)
                .load(artUrlForWeatherCondition)
                .asBitmap()
                .error(artResourceForWeatherCondition)
                .into(largeIconWidth, largeIconHeight)
                .get();

        int iconId = Utility.getIconResourceForWeatherCondition(weatherId);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                .setColor(getResources().getColor(R.color.sunshine_light_blue))
                .setLargeIcon(downloadedLargeIcon)
                .setSmallIcon(iconId)
                .setContentTitle(title)
                .setContentText(summary);

        NotificationManager notificationManager = ((NotificationManager) getSystemService(NOTIFICATION_SERVICE));
        notificationManager.notify(3005, mBuilder.build());
    }
}
