/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.android.sunshine.app;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.Preference;
import android.preference.PreferenceManager;
import android.text.format.Time;

import com.example.android.sunshine.app.sync.SunshineSyncAdapter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Utility {

    private static final String STORM = "storm";
    private static final String LIGHT_RAIN = "light_rain";
    private static final String RAIN = "rain";
    private static final String SNOW = "snow";
    private static final String FOG = "fog";
    private static final String CLEAR = "clear";
    private static final String LIGHT_CLOUDS = "light_clouds";
    private static final String CLOUDS = "clouds";

    public static String getPreferredLocation(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString(context.getString(R.string.pref_location_key),
                context.getString(R.string.pref_location_default));
    }

    public static boolean isMetric(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString(context.getString(R.string.pref_units_key),
                context.getString(R.string.pref_units_metric))
                .equals(context.getString(R.string.pref_units_metric));
    }

    public static String formatTemperature(Context context, double temperature) {
        // Data stored in Celsius by default.  If user prefers to see in Fahrenheit, convert
        // the values here.
        String suffix = "\u00B0";
        if (!isMetric(context)) {
            temperature = (temperature * 1.8) + 32;
        }

        // For presentation, assume the user doesn't care about tenths of a degree.
        return String.format(context.getString(R.string.format_temperature), temperature);
    }

    static String formatDate(long dateInMilliseconds) {
        Date date = new Date(dateInMilliseconds);
        return DateFormat.getDateInstance().format(date);
    }

    // Format used for storing dates in the database.  ALso used for converting those strings
    // back into date objects for comparison/processing.
    public static final String DATE_FORMAT = "yyyyMMdd";

    /**
     * Helper method to convert the database representation of the date into something to display
     * to users.  As classy and polished a user experience as "20140102" is, we can do better.
     *
     * @param context      Context to use for resource localization
     * @param dateInMillis The date in milliseconds
     * @return a user-friendly representation of the date.
     */
    public static String getFriendlyDayString(Context context, long dateInMillis) {
        // The day string for forecast uses the following logic:
        // For today: "Today, June 8"
        // For tomorrow:  "Tomorrow"
        // For the next 5 days: "Wednesday" (just the day name)
        // For all days after that: "Mon Jun 8"

        Time time = new Time();
        time.setToNow();
        long currentTime = System.currentTimeMillis();
        int julianDay = Time.getJulianDay(dateInMillis, time.gmtoff);
        int currentJulianDay = Time.getJulianDay(currentTime, time.gmtoff);

        // If the date we're building the String for is today's date, the format
        // is "Today, June 24"
        if (julianDay == currentJulianDay) {
            String today = context.getString(R.string.today);
            int formatId = R.string.format_full_friendly_date;
            return String.format(context.getString(
                    formatId,
                    today,
                    getFormattedMonthDay(context, dateInMillis)));
        } else if (julianDay < currentJulianDay + 7) {
            // If the input date is less than a week in the future, just return the day name.
            return getDayName(context, dateInMillis);
        } else {
            // Otherwise, use the form "Mon Jun 3"
            SimpleDateFormat shortenedDateFormat = new SimpleDateFormat("EEE MMM dd");
            return shortenedDateFormat.format(dateInMillis);
        }
    }

    /**
     * Given a day, returns just the name to use for that day.
     * E.g "today", "tomorrow", "wednesday".
     *
     * @param context      Context to use for resource localization
     * @param dateInMillis The date in milliseconds
     * @return
     */
    public static String getDayName(Context context, long dateInMillis) {
        // If the date is today, return the localized version of "Today" instead of the actual
        // day name.

        Time t = new Time();
        t.setToNow();
        int julianDay = Time.getJulianDay(dateInMillis, t.gmtoff);
        int currentJulianDay = Time.getJulianDay(System.currentTimeMillis(), t.gmtoff);
        if (julianDay == currentJulianDay) {
            return context.getString(R.string.today);
        } else if (julianDay == currentJulianDay + 1) {
            return context.getString(R.string.tomorrow);
        } else {
            Time time = new Time();
            time.setToNow();
            // Otherwise, the format is just the day of the week (e.g "Wednesday".
            SimpleDateFormat dayFormat = new SimpleDateFormat("EEEE");
            return dayFormat.format(dateInMillis);
        }
    }

    /**
     * Converts db date format to the format "Month day", e.g "June 24".
     *
     * @param context      Context to use for resource localization
     * @param dateInMillis The db formatted date string, expected to be of the form specified
     *                     in Utility.DATE_FORMAT
     * @return The day in the form of a string formatted "December 6"
     */
    public static String getFormattedMonthDay(Context context, long dateInMillis) {
        Time time = new Time();
        time.setToNow();
        SimpleDateFormat dbDateFormat = new SimpleDateFormat(Utility.DATE_FORMAT);
        SimpleDateFormat monthDayFormat = new SimpleDateFormat("MMMM dd");
        String monthDayString = monthDayFormat.format(dateInMillis);
        return monthDayString;
    }

    public static String getFormattedWind(Context context, float windSpeed, float degrees) {
        int windFormat;
        if (Utility.isMetric(context)) {
            windFormat = R.string.format_wind_kmh;
        } else {
            windFormat = R.string.format_wind_mph;
            windSpeed = .621371192237334f * windSpeed;
        }

        // From wind direction in degrees, determine compass direction as a string (e.g NW)
        // You know what's fun, writing really long if/else statements with tons of possible
        // conditions.  Seriously, try it!
        String direction = "Unknown";
        if (degrees >= 337.5 || degrees < 22.5) {
            direction = "N";
        } else if (degrees >= 22.5 && degrees < 67.5) {
            direction = "NE";
        } else if (degrees >= 67.5 && degrees < 112.5) {
            direction = "E";
        } else if (degrees >= 112.5 && degrees < 157.5) {
            direction = "SE";
        } else if (degrees >= 157.5 && degrees < 202.5) {
            direction = "S";
        } else if (degrees >= 202.5 && degrees < 247.5) {
            direction = "SW";
        } else if (degrees >= 247.5 && degrees < 292.5) {
            direction = "W";
        } else if (degrees >= 292.5 && degrees < 337.5) {
            direction = "NW";
        }
        return String.format(context.getString(windFormat), windSpeed, direction);
    }

    /**
     * Helper method to provide the icon resource id according to the weather condition id returned
     * by the OpenWeatherMap call.
     *
     * @param weatherId from OpenWeatherMap API response
     * @return resource id for the corresponding icon. -1 if no relation is found.
     */
    public static int getIconResourceForWeatherCondition(int weatherId) {
        // Based on weather code data found at:
        // http://bugs.openweathermap.org/projects/api/wiki/Weather_Condition_Codes
        if (weatherId >= 200 && weatherId <= 232) {
            return R.drawable.ic_storm;
        } else if (weatherId >= 300 && weatherId <= 321) {
            return R.drawable.ic_light_rain;
        } else if (weatherId >= 500 && weatherId <= 504) {
            return R.drawable.ic_rain;
        } else if (weatherId == 511) {
            return R.drawable.ic_snow;
        } else if (weatherId >= 520 && weatherId <= 531) {
            return R.drawable.ic_rain;
        } else if (weatherId >= 600 && weatherId <= 622) {
            return R.drawable.ic_snow;
        } else if (weatherId >= 701 && weatherId <= 761) {
            return R.drawable.ic_fog;
        } else if (weatherId == 761 || weatherId == 781) {
            return R.drawable.ic_storm;
        } else if (weatherId == 800) {
            return R.drawable.ic_clear;
        } else if (weatherId == 801) {
            return R.drawable.ic_light_clouds;
        } else if (weatherId >= 802 && weatherId <= 804) {
            return R.drawable.ic_cloudy;
        }
        return -1;
    }

    /**
     * Helper method to provide the art resource id according to the weather condition id returned
     * by the OpenWeatherMap call.
     *
     * @param weatherId from OpenWeatherMap API response
     * @return resource id for the corresponding icon. -1 if no relation is found.
     */
    public static int getArtResourceForWeatherCondition(int weatherId) {
        // Based on weather code data found at:
        // http://bugs.openweathermap.org/projects/api/wiki/Weather_Condition_Codes
        if (weatherId >= 200 && weatherId <= 232) {
            return R.drawable.art_storm;
        } else if (weatherId >= 300 && weatherId <= 321) {
            return R.drawable.art_light_rain;
        } else if (weatherId >= 500 && weatherId <= 504) {
            return R.drawable.art_rain;
        } else if (weatherId == 511) {
            return R.drawable.art_snow;
        } else if (weatherId >= 520 && weatherId <= 531) {
            return R.drawable.art_rain;
        } else if (weatherId >= 600 && weatherId <= 622) {
            return R.drawable.art_snow;
        } else if (weatherId >= 701 && weatherId <= 761) {
            return R.drawable.art_fog;
        } else if (weatherId == 761 || weatherId == 781) {
            return R.drawable.art_storm;
        } else if (weatherId == 800) {
            return R.drawable.art_clear;
        } else if (weatherId == 801) {
            return R.drawable.art_light_clouds;
        } else if (weatherId >= 802 && weatherId <= 804) {
            return R.drawable.art_clouds;
        }
        return -1;
    }

    private static String getPreferredIconPack(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        String iconPackKey = context.getString(R.string.pref_art_pack_key);
        String selectedIconPack = sharedPreferences.getString(iconPackKey, "");
        return selectedIconPack;
    }

    public static String getArtUrlForWeatherCondition(Context context, int weatherId) {
        // Based on weather code data found at:
        // http://bugs.openweathermap.org/projects/api/wiki/Weather_Condition_Codes
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        String artPackKey = context.getString(R.string.pref_art_pack_key);
        String formatArtUrl = sharedPreferences.getString(artPackKey, context.getString(R.string.pref_art_pack_sunshine));

        if (weatherId >= 200 && weatherId <= 232) {
            return String.format(Locale.US, formatArtUrl, STORM);
        } else if (weatherId >= 300 && weatherId <= 321) {
            return String.format(Locale.US, formatArtUrl, LIGHT_RAIN);
        } else if (weatherId >= 500 && weatherId <= 504) {
            return String.format(Locale.US, formatArtUrl, RAIN);
        } else if (weatherId == 511) {
            return String.format(Locale.US, formatArtUrl, SNOW);
        } else if (weatherId >= 520 && weatherId <= 531) {
            return String.format(Locale.US, formatArtUrl, RAIN);
        } else if (weatherId >= 600 && weatherId <= 622) {
            return String.format(Locale.US, formatArtUrl, SNOW);
        } else if (weatherId >= 701 && weatherId <= 761) {
            return String.format(Locale.US, formatArtUrl, FOG);
        } else if (weatherId == 761 || weatherId == 781) {
            return String.format(Locale.US, formatArtUrl, STORM);
        } else if (weatherId == 800) {
            return String.format(Locale.US, formatArtUrl, CLEAR);
        } else if (weatherId == 801) {
            return String.format(Locale.US, formatArtUrl, LIGHT_CLOUDS);
        } else if (weatherId >= 802 && weatherId <= 804) {
            return String.format(Locale.US, formatArtUrl, CLOUDS);
        }
        return null;
    }

    public static Integer getWeatherDescriptionResource(Integer weatherId) {
        Integer weatherDescriptionResource;
        switch (weatherId) {
            case 200:
                weatherDescriptionResource = R.string.weather_code_200;
                break;
            case 201:
                weatherDescriptionResource = R.string.weather_code_201;
                break;
            case 202:
                weatherDescriptionResource = R.string.weather_code_202;
                break;
            case 210:
                weatherDescriptionResource = R.string.weather_code_210;
                break;
            case 211:
                weatherDescriptionResource = R.string.weather_code_211;
                break;
            case 212:
                weatherDescriptionResource = R.string.weather_code_212;
                break;
            case 221:
                weatherDescriptionResource = R.string.weather_code_221;
                break;
            case 230:
                weatherDescriptionResource = R.string.weather_code_230;
                break;
            case 231:
                weatherDescriptionResource = R.string.weather_code_231;
                break;
            case 232:
                weatherDescriptionResource = R.string.weather_code_232;
                break;
            case 300:
                weatherDescriptionResource = R.string.weather_code_300;
                break;
            case 301:
                weatherDescriptionResource = R.string.weather_code_301;
                break;
            case 302:
                weatherDescriptionResource = R.string.weather_code_302;
                break;
            case 310:
                weatherDescriptionResource = R.string.weather_code_310;
                break;
            case 311:
                weatherDescriptionResource = R.string.weather_code_311;
                break;
            case 312:
                weatherDescriptionResource = R.string.weather_code_312;
                break;
            case 313:
                weatherDescriptionResource = R.string.weather_code_313;
                break;
            case 314:
                weatherDescriptionResource = R.string.weather_code_314;
                break;
            case 321:
                weatherDescriptionResource = R.string.weather_code_321;
                break;
            case 500:
                weatherDescriptionResource = R.string.weather_code_500;
                break;
            case 501:
                weatherDescriptionResource = R.string.weather_code_501;
                break;
            case 502:
                weatherDescriptionResource = R.string.weather_code_502;
                break;
            case 503:
                weatherDescriptionResource = R.string.weather_code_503;
                break;
            case 504:
                weatherDescriptionResource = R.string.weather_code_504;
                break;
            case 511:
                weatherDescriptionResource = R.string.weather_code_511;
                break;
            case 520:
                weatherDescriptionResource = R.string.weather_code_520;
                break;
            case 521:
                weatherDescriptionResource = R.string.weather_code_521;
                break;
            case 522:
                weatherDescriptionResource = R.string.weather_code_522;
                break;
            case 531:
                weatherDescriptionResource = R.string.weather_code_531;
                break;
            case 600:
                weatherDescriptionResource = R.string.weather_code_600;
                break;
            case 601:
                weatherDescriptionResource = R.string.weather_code_601;
                break;
            case 602:
                weatherDescriptionResource = R.string.weather_code_602;
                break;
            case 611:
                weatherDescriptionResource = R.string.weather_code_611;
                break;
            case 612:
                weatherDescriptionResource = R.string.weather_code_612;
                break;
            case 615:
                weatherDescriptionResource = R.string.weather_code_615;
                break;
            case 616:
                weatherDescriptionResource = R.string.weather_code_616;
                break;
            case 620:
                weatherDescriptionResource = R.string.weather_code_620;
                break;
            case 621:
                weatherDescriptionResource = R.string.weather_code_621;
                break;
            case 622:
                weatherDescriptionResource = R.string.weather_code_622;
                break;
            case 701:
                weatherDescriptionResource = R.string.weather_code_701;
                break;
            case 711:
                weatherDescriptionResource = R.string.weather_code_711;
                break;
            case 721:
                weatherDescriptionResource = R.string.weather_code_721;
                break;
            case 731:
                weatherDescriptionResource = R.string.weather_code_731;
                break;
            case 741:
                weatherDescriptionResource = R.string.weather_code_741;
                break;
            case 751:
                weatherDescriptionResource = R.string.weather_code_751;
                break;
            case 761:
                weatherDescriptionResource = R.string.weather_code_761;
                break;
            case 762:
                weatherDescriptionResource = R.string.weather_code_762;
                break;
            case 771:
                weatherDescriptionResource = R.string.weather_code_771;
                break;
            case 781:
                weatherDescriptionResource = R.string.weather_code_781;
                break;
            case 800:
                weatherDescriptionResource = R.string.weather_code_800;
                break;
            case 801:
                weatherDescriptionResource = R.string.weather_code_801;
                break;
            case 802:
                weatherDescriptionResource = R.string.weather_code_802;
                break;
            case 803:
                weatherDescriptionResource = R.string.weather_code_803;
                break;
            case 804:
                weatherDescriptionResource = R.string.weather_code_804;
                break;
            case 900:
                weatherDescriptionResource = R.string.weather_code_900;
                break;
            case 901:
                weatherDescriptionResource = R.string.weather_code_901;
                break;
            case 902:
                weatherDescriptionResource = R.string.weather_code_902;
                break;
            case 903:
                weatherDescriptionResource = R.string.weather_code_903;
                break;
            case 904:
                weatherDescriptionResource = R.string.weather_code_904;
                break;
            case 905:
                weatherDescriptionResource = R.string.weather_code_905;
                break;
            case 906:
                weatherDescriptionResource = R.string.weather_code_906;
                break;
            case 951:
                weatherDescriptionResource = R.string.weather_code_951;
                break;
            case 952:
                weatherDescriptionResource = R.string.weather_code_952;
                break;
            case 953:
                weatherDescriptionResource = R.string.weather_code_953;
                break;
            case 954:
                weatherDescriptionResource = R.string.weather_code_954;
                break;
            case 955:
                weatherDescriptionResource = R.string.weather_code_955;
                break;
            case 956:
                weatherDescriptionResource = R.string.weather_code_956;
                break;
            case 957:
                weatherDescriptionResource = R.string.weather_code_957;
                break;
            case 958:
                weatherDescriptionResource = R.string.weather_code_958;
                break;
            case 959:
                weatherDescriptionResource = R.string.weather_code_959;
                break;
            case 960:
                weatherDescriptionResource = R.string.weather_code_960;
                break;
            case 961:
                weatherDescriptionResource = R.string.weather_code_961;
                break;
            case 962:
                weatherDescriptionResource = R.string.weather_code_962;
                break;
            default:
                weatherDescriptionResource = R.string.weather_code_500;
                break;
        }
        return weatherDescriptionResource;
    }

    @SuppressWarnings("ResourceType")
    public static
    @SunshineSyncAdapter.LocationStatus
    int getLocationStatus(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        String locationStatusKey = context.getString(R.string.pref_location_status_key);
        return sharedPreferences.getInt(locationStatusKey, SunshineSyncAdapter.LOCATION_STATUS_UNKNOWN);
    }

    public static void setLocationStatusAsUnknown(Context context, Preference preference) {
        SharedPreferences.Editor editor = preference.getEditor();
        String locationStatusKey = context.getString(R.string.pref_location_status_key);
        editor.putInt(locationStatusKey, SunshineSyncAdapter.LOCATION_STATUS_UNKNOWN);
        editor.apply();
    }
}