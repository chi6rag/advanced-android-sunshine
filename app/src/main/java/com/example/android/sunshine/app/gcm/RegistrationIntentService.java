package com.example.android.sunshine.app.gcm;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.example.android.sunshine.app.R;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import java.io.IOException;

public class RegistrationIntentService extends IntentService {
    private static final String REGISTRATION_INTENT_SERVICE = "RegistrationIntentService";

    public RegistrationIntentService() {
        super(REGISTRATION_INTENT_SERVICE);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        try {
            InstanceID instanceID = InstanceID.getInstance(this);
            String token = instanceID.getToken(getString(R.string.gcm_defaultSenderId), GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
            Log.i("chi6rag", "GCM Registration Token: " + token);
            setRegistrationKeySent(sharedPreferences, Boolean.TRUE);
        } catch (IOException e) {
            e.printStackTrace();
            setRegistrationKeySent(sharedPreferences, Boolean.FALSE);
        }
    }

    private void setRegistrationKeySent(SharedPreferences sharedPreferences, Boolean hasSentRegistrationKey) {
        sharedPreferences.edit().putBoolean(getString(R.string.sent_registration_to_server_key), hasSentRegistrationKey).apply();
    }
}
