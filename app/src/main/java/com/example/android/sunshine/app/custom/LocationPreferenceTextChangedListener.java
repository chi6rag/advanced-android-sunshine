package com.example.android.sunshine.app.custom;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.Button;

public class LocationPreferenceTextChangedListener implements TextWatcher {
    private final int minimumLength;
    private final Button buttonOk;

    public LocationPreferenceTextChangedListener(Button button, int minimumLength) {
        this.buttonOk = button;
        this.minimumLength = minimumLength;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        enableButtonOnlyIfTextLengthGreaterThanMinimumLength(editable, buttonOk);
    }

    private void enableButtonOnlyIfTextLengthGreaterThanMinimumLength(Editable editable, Button buttonOk) {
        if (editable.length() < minimumLength) {
            buttonOk.setEnabled(false);
        } else {
            buttonOk.setEnabled(true);
        }
    }
}
