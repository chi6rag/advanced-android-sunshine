package com.example.android.sunshine.app.gcm;

import android.content.Intent;

import com.google.android.gms.iid.InstanceIDListenerService;

public class SunshineInstanceIdListenerService extends InstanceIDListenerService {
    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        Intent registrationServiceIntent = new Intent(this, RegistrationIntentService.class);
        startService(registrationServiceIntent);
    }
}
